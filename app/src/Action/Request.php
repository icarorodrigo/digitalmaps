<?php

namespace App\Action;

class Request{

    public $body;

    public function setBody($arg){
        $this->body = $arg;
    }

    public function getParsedBody(){
        return $this->body;
    }
}