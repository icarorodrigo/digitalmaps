<?php
namespace App\Action;

use PDO;

class Conexao{

   private $usuario_banco = "admin";
   private $senha_banco = "123456";
   private $servidore_banco = "mysql";
   private $base_dados = "digitalMaps";
   private $porta_banco = "3306";
   public  $erro;
   public $stmt;

   public function getConection(){

        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => false,
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];

        $pdo = new PDO("mysql:host={$this->servidore_banco};port={$this->porta_banco};dbname={$this->base_dados}", $this->usuario_banco, $this->senha_banco,$options);

        return $pdo;

   }

}