<?php

namespace App\Controller;

use App\Services\CategoriaService;

class CategoriaController
{
   protected $container;


   public function create($request, $response, $args) {
        $categoriaServices  =  new CategoriaService();
        $return  = $categoriaServices->create($request,$response, $args);
        return $return;
   }

   public function get($request, $response, $args) {
        $categoriaServices  =  new CategoriaService();
        $return  = $categoriaServices->get($request, $response, $args);
        return $return;
   }

   public function getId($request, $response, $args) {
        $categoriaServices  =  new CategoriaService();
        $return  = $categoriaServices->getId($request, $response, $args);
        return $return;
    }

    public function update($request, $response, $args) {
        $categoriaServices  =  new CategoriaService();
        $return  = $categoriaServices->update($request, $response, $args);
        return $return;
    }

    public function delete($request, $response, $args) {
            $categoriaServices  =  new CategoriaService();
            $return  = $categoriaServices->delete($request, $response, $args);
            return $return;

    }
}