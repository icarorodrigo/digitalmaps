<?php

namespace App\Controller;

use App\Services\PontoInteresseService;

class PontoInteresseController
{
   protected $container;


    public function create($request, $response, $args) {
      $pontoInteresseServices  =  new PontoInteresseService();
      $return  = $pontoInteresseServices->create($request,$response, $args);

      return $return;
    }

    public function get($request, $response, $args) {
        $pontoInteresseServices  =  new PontoInteresseService();
        $return  = $pontoInteresseServices->get($request, $response, $args);
        return $return;
    }

    public function getId($request, $response, $args) {
        $pontoInteresseServices  =  new PontoInteresseService();
        $return  = $pontoInteresseServices->getId($request, $response, $args);
        return $return;
    }

    public function getProximos($request, $response, $args) {
        $pontoInteresseServices  =  new PontoInteresseService();
        $return  = $pontoInteresseServices->getProximos($request, $response, $args);
        return $return;
    }

    public function update($request, $response, $args) {
        $pontoInteresseServices  =  new PontoInteresseService();
        $return  = $pontoInteresseServices->update($request, $response, $args);
        return $return;
    }

    public function delete($request, $response, $args) {
        $pontoInteresseServices  =  new PontoInteresseService();
        $return  = $pontoInteresseServices->delete($request, $response, $args);
        return $return;
    }
}