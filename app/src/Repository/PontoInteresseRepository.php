<?php
namespace App\Repository;

interface PontoInteresseRepository {
 
    public function insert($body);
    public function get();
    public function getId($id);
    public function update($body,$id);
    public function delete($id);    
}