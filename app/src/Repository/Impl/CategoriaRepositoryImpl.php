<?php
namespace App\Repository\Impl;

use App\Repository\CategoriaRepository;
use PDO;

class CategoriaRepositoryImpl implements CategoriaRepository{

    private $conection;
    
    public function __construct($conection){
            $this->conection = $conection;
    }

    public function insert($body){
        $pdo = $this->conection->getConection();
        $sql = "INSERT INTO categoria (nome,descricao,possui_horario) values (:nome,:descricao,:possui_horario)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(":nome",$body['nome'],PDO::PARAM_STR);
        $stmt->bindValue(":descricao",$body['descricao'],PDO::PARAM_STR);
        $stmt->bindValue(":possui_horario",$body["possui_horario"],PDO::PARAM_INT);
        $stmt->execute();
        return $pdo->lastInsertId();
    }

    public function get(){
        $pdo = $this->conection->getConection();
        $sql = "SELECT id,nome,descricao,possui_horario FROM categoria";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_OBJ);
       
    }

    public function getId($id){
        $pdo = $this->conection->getConection();
        $sql = "SELECT id,nome,descricao,possui_horario FROM categoria WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(":id",$id,PDO::PARAM_INT);
        $stmt->execute();
        return  $stmt->fetch(PDO::FETCH_OBJ);
       
    }

    public function update($body,$id){
        $pdo = $this->conection->getConection();
        $sql = "UPDATE categoria SET nome = :nome, descricao = :descricao, possui_horario = :possui_horario WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(":nome",$body["nome"],PDO::PARAM_STR);
        $stmt->bindValue(":descricao",$body["descricao"],PDO::PARAM_STR);
        $stmt->bindValue(":possui_horario",$body["possui_horario"],PDO::PARAM_INT);
        $stmt->bindValue(":id",$id,PDO::PARAM_INT);
        $rs = $stmt->execute();
        return  $rs;
       
    }

    public function delete($id){
        $pdo = $this->conection->getConection();
        $sql = "DELETE FROM categoria WHERE id = $id";
        $stmt = $pdo->prepare($sql);
        $rs = $stmt->execute();
        return  $rs;
       
    }
}