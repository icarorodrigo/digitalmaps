<?php
namespace App\Repository\Impl;

use App\Repository\PontoInteresseRepository;
use PDO;

class PontoInteresseRepositoryImpl implements PontoInteresseRepository{

    private $conection;
    
    public function __construct($conection){
            $this->conection = $conection;
    }

    public function insert($body){
        $pdo = $this->conection->getConection();
        $sql = "INSERT INTO ponto_interesse (id_categoria, nome, descricao, coordenada_x, coordenada_y, abertura, fechamento) 
                                    VALUES 
                                    (:id_categoria, :nome, :descricao, :coordenada_x, :coordenada_y, :abertura, :fechamento)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(":id_categoria",$body['id_categoria'],PDO::PARAM_INT);
        $stmt->bindValue(":nome",$body['nome'],PDO::PARAM_STR);
        $stmt->bindValue(":descricao",$body['descricao'],PDO::PARAM_STR);
        $stmt->bindValue(":coordenada_x",$body["coordenada_x"],PDO::PARAM_INT);
        $stmt->bindValue(":coordenada_y",$body["coordenada_y"],PDO::PARAM_INT);
        $stmt->bindValue(":abertura",$body["abertura"],PDO::PARAM_STR);
        $stmt->bindValue(":fechamento",$body["fechamento"],PDO::PARAM_STR);
        $stmt->execute();
        return $pdo->lastInsertId();
    }

    public function get(){
        $pdo = $this->conection->getConection();
        $sql = "SELECT id,id_categoria, nome, descricao, coordenada_x, coordenada_y, abertura, fechamento FROM ponto_interesse";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return  $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function getId($id){
        $pdo = $this->conection->getConection();
        $sql = "SELECT id,id_categoria, nome, descricao, coordenada_x, coordenada_y, abertura, fechamento FROM ponto_interesse WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(":id",$id,PDO::PARAM_INT);
        $stmt->execute();
        return  $stmt->fetch(PDO::FETCH_OBJ);
    }

    public function getIdCategoria($idCategoria){
        $pdo = $this->conection->getConection();
        $sql = "SELECT id,id_categoria, nome, descricao, coordenada_x, coordenada_y, abertura, fechamento FROM ponto_interesse WHERE id_categoria = :idCategoria";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(":idCategoria",$idCategoria,PDO::PARAM_INT);
        $stmt->execute();
        return  $stmt->fetch(PDO::FETCH_OBJ);
    }

    public function update($body,$id){
        $pdo = $this->conection->getConection();
        $sql = "UPDATE ponto_interesse 
                    SET id_categoria = :id_categoria, nome = :nome, descricao = :descricao
                        ,coordenada_x = :coordenada_x, coordenada_y = :coordenada_y, abertura = :abertura, fechamento = :fechamento
                WHERE id = :id";

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(":id_categoria",$body['id_categoria'],PDO::PARAM_INT);
        $stmt->bindValue(":nome",$body['nome'],PDO::PARAM_STR);
        $stmt->bindValue(":descricao",$body['descricao'],PDO::PARAM_STR);
        $stmt->bindValue(":coordenada_x",$body["coordenada_x"],PDO::PARAM_INT);
        $stmt->bindValue(":coordenada_y",$body["coordenada_y"],PDO::PARAM_INT);
        $stmt->bindValue(":abertura",$body["abertura"],PDO::PARAM_STR);
        $stmt->bindValue(":fechamento",$body["fechamento"],PDO::PARAM_STR);
        $stmt->bindValue(":id",$id,PDO::PARAM_INT);
        $rs = $stmt->execute();
        return  $rs;
    }

    public function delete($id){
        $pdo = $this->conection->getConection();
        $sql = "DELETE FROM ponto_interesse WHERE id = $id";
        $stmt = $pdo->prepare($sql);
        $rs = $stmt->execute();
        return  $rs;
    }
}