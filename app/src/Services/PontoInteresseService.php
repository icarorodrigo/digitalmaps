<?php
namespace App\Services;

use App\Repository\Impl\PontoInteresseRepositoryImpl;
use App\Repository\Impl\CategoriaRepositoryImpl;
use App\Action\Conexao;
use Exception;
use PDOException;



class PontoInteresseService{

    private $conexao;

    public function __construct(){
        $this->conexao = new Conexao();
    }

    public function create($request,$response, $args){
        try{
            $return = "";
            $pontoInteresseRepository = new PontoInteresseRepositoryImpl($this->conexao);
            $body = $request->getParsedBody();
            
            if($this->possuiHorarioFuncionamento($body['id_categoria']))
                $this->validaBodyEntrada($body);

            $idPontoInteresse =  $pontoInteresseRepository->insert($body);

            if($idPontoInteresse)
                $return =  $pontoInteresseRepository->getId($idPontoInteresse);

            return json_encode($return);
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }
    }

    public function possuiHorarioFuncionamento($idCategoria){
        $categoriaRepository = new CategoriaRepositoryImpl($this->conexao);
        return $categoriaRepository->getId($idCategoria)->possui_horario;
    }

    public function validaBodyEntrada($body){
        if(empty($body['abertura']) || empty($body['fechamento']))
            throw new Exception("O horario de funcionamento deve ser definido, parametros faltantes Ex: {\"abertura\":\"10:00\",\"fechamento\": \"18:00\"}",400);
        
        return true;
    }

    public function get($request,$response, $args){
        try{
            $pontoInteresseRepository = new PontoInteresseRepositoryImpl($this->conexao);
            $return =  $pontoInteresseRepository->get();
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }
        
        return json_encode($return);
    }

    public function getId($request,$response, $args){
        try{
            $id = $args['id'];
            $pontoInteresseRepository = new PontoInteresseRepositoryImpl($this->conexao);
            $return =  $pontoInteresseRepository->getId($id);
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }
        return json_encode($return);
    }

    public function getProximos($request,$response, $args){
        try{
            $pontoInteresseRepository = new PontoInteresseRepositoryImpl($this->conexao);
            $pontosInteresse =  $pontoInteresseRepository->get();
            $body = $request->getParsedBody();
            $proximos = $this->buscarProximos($pontosInteresse, $body);

        }
        catch(Exception $e){
            throw new Exception($e->getMessage(),400);
        }
        return json_encode($proximos);
    }

    public function buscarProximos($pontosInteresse,$body){
        $proximos =  Array();
        $indiceProximos = 0;
        foreach($pontosInteresse as $indice => $valor){
            if(!$this->calcularDistancia($valor, $body))
                continue;

            $proximos[$indiceProximos] = $this->tratarRetorno($valor,$body);
            $indiceProximos++;
        }
        return $proximos;
    }

    public function calcularDistancia($pontosInteresse, $body){
        $distanciaX = abs($pontosInteresse->coordenada_x - $body['coordenada_x']);
        $distanciaY = abs($pontosInteresse->coordenada_y - $body['coordenada_y']);
        $distanciaTotal = $distanciaX + $distanciaY;
        
        if($distanciaTotal > $body['metros'])
            return false;
        
        return true;
    }

    public function tratarRetorno($pontosInteresse,$body){
        $status = "Aberto";
        $proximos["nome"] = $pontosInteresse->nome;
        
        if($pontosInteresse->fechamento)
            $status = ( strtotime($pontosInteresse->fechamento) < strtotime($body["hora"]) ? "Aberto":"Fechado");

        $proximos["status"] = $status;

        return $proximos;
    }

    public function update($request,$response, $args){
        try{
            $id = $args['id'];
            $return = "";

            $pontoInteresseRepository = new PontoInteresseRepositoryImpl($this->conexao);
            $body = $request->getParsedBody();

            $categoriaRepository = new CategoriaRepositoryImpl($this->conexao);
            if(empty($categoriaRepository->getId($body['id_categoria'])))
                throw new Exception("Categoria informada inválida.",400);

            $update =  $pontoInteresseRepository->update($body,$id);
            if($update)
                $return =  $pontoInteresseRepository->getId($id);
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }
        catch(Exception $e){
            throw new Exception($e->getMessage(),400);
        }
        
        return json_encode($return);
    }

    public function delete($request,$response, $args){
        try{
            $id = $args['id'];
            $pontoInteresseRepository = new PontoInteresseRepositoryImpl($this->conexao);
            $return =  $pontoInteresseRepository->delete($id);

            if($return)
                $messageReturn = "Registro id $id removido com sucesso";
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }
        catch(Exception $e){
            throw new Exception($e->getMessage(),400);
        }
        return json_encode($messageReturn);
    }
}

