<?php
namespace App\Services;

use App\Repository\Impl\CategoriaRepositoryImpl;
use App\Repository\Impl\PontoInteresseRepositoryImpl;
use App\Action\Conexao;
use Exception;
use PDOException;

class CategoriaService{

    private $conexao;

    public function __construct(){

        $this->conexao = new Conexao();
    }

    public function create($request,$response, $args){
        try{
            $return = "";
            $categoriaRepository = new CategoriaRepositoryImpl($this->conexao);
            $body = $request->getParsedBody();
            $idCategoria =  $categoriaRepository->insert($body);
            if($idCategoria)
                $return =  $categoriaRepository->getId($idCategoria);
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }

        return json_encode($return);
    }

    public function get($request,$response, $args){
        try{
            $categoriaRepository = new CategoriaRepositoryImpl($this->conexao);
            $return =  $categoriaRepository->get();
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }
        
        return json_encode($return);
    }

    public function getId($request,$response, $args){
        try{
            $id = $args['id'];
            $categoriaRepository = new CategoriaRepositoryImpl($this->conexao);
            $return =  $categoriaRepository->getId($id);
            
            return json_encode($return);
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }
    }

    public function update($request,$response, $args){
        try{
            $id = $args['id'];
            $return = "";

            $categoriaRepository = new CategoriaRepositoryImpl($this->conexao);
            $body = $request->getParsedBody();

            $update =  $categoriaRepository->update($body,$id);
            if($update)
                $return =  $categoriaRepository->getId($id);
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }
        catch(Exception $e){
            throw new Exception($e->getMessage(),400);
        }

        return json_encode($return);
    }

    public function delete($request,$response, $args){
        try{
            $id = $args['id'];

            $pontoInteresseRepository = new PontoInteresseRepositoryImpl($this->conexao);
            if($pontoInteresseRepository->getIdCategoria($id))
                throw new Exception("Erro: Existem pontos de interesse vinculados a essa categoria. você deve excluir os pontos primeiro",400);

            $categoriaRepository = new CategoriaRepositoryImpl($this->conexao);
            $return =  $categoriaRepository->delete($id);
            if($return)
                $messageReturn = "Registro id $id removido com sucesso";
        }
        catch(PDOException $e){
            throw new Exception($e->getMessage(),400);
        }
        catch(Exception $e){
            throw new Exception($e->getMessage(),400);
        }
            
        return json_encode($messageReturn);
    }
}

