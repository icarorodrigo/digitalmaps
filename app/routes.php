<?php

$app->get('/ponto_interesse', \PontoInteresseController::class . ':get')->setName("pontoInteresse");
$app->get('/ponto_interesse/listar_proximos', \PontoInteresseController::class . ':getProximos');
$app->get('/ponto_interesse/{id}', \PontoInteresseController::class . ':getId')->setName("pontoInteresseId");
$app->post('/ponto_interesse', \PontoInteresseController::class . ':create');
$app->put('/ponto_interesse/{id}', \PontoInteresseController::class . ':update');
$app->delete('/ponto_interesse/{id}', \PontoInteresseController::class . ':delete');

$app->get('/categoria', \CategoriaController::class . ':get')->setName("categoria");
$app->get('/categoria/{id}', \CategoriaController::class . ':getId')->setName("categoriaId");
$app->post('/categoria', \CategoriaController::class . ':create');
$app->put('/categoria/{id}', \CategoriaController::class . ':update');
$app->delete('/categoria/{id}', \CategoriaController::class . ':delete');

