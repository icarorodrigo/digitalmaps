FROM php:7.4-apache

# Enable apache rewrite
COPY ./apache/000-default.conf /etc/apache2/sites-available/000-default.conf

RUN apt-get -y update

RUN apt-get install -y imagemagick 
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev  zip  libzip-dev\
        vim

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN docker-php-ext-install gd && docker-php-ext-enable gd
RUN docker-php-ext-install zip && docker-php-ext-enable zip
RUN docker-php-ext-install pdo pdo_mysql

COPY ./ /var/www/html/LuizaLabs/digitalmaps

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --no-scripts

WORKDIR /var/www/html/LuizaLabs/digitalmaps/
RUN composer update
RUN composer create-project --no-interaction --stability=dev akrabat/slim3-skeleton digitalmaps
RUN composer dumpautoload -o
RUN chmod 775 -R /var/www/html/LuizaLabs/digitalmaps/log
RUN chmod 775 -R /var/www/html/LuizaLabs/digitalmaps/cache
RUN a2enmod rewrite
RUN service apache2 restart

EXPOSE 8080