# Desafio Técnico LuizaLabs

O ambiente de desenvolvimento foi construindo em um conteiner utilizando docker, para poder utilizar o conteiner é necessário que o
docker esteja instalado.

A código em php esta estruturado pelo framework Slim na versão 3

A banco de dados utilizado é o MYSQL e a estrutura da base  está no arquivo BANCO.SQL.

O conteiner vai disponibilizar a conexão ao banco de dados no seguinte endereço 127.0.0.1:33306, usuario: admin, senha: 123456


## 1º Instalação do Docker (ambiente linux):

    O docker pode ser instalado utilizando o tutorial de instalação disponibilizado no link a baixo.
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-pt

### Run it:
Apos a instalação do docker, certifique-se que tenha o git instalado e clone o projeto.
Acesse o projeto e rode o comando a baixo para poder construir o projeto.

1. `sudo docker-compose up --build
2.  os endpoints estrão disponíveis na url http://localhost:8080

## Endpoints
Lista de endpoints disponíveis\n 

Categorias (são utilizadas para poder categorizar os pontos de interesse, possui uma flag que indica se possui horario de funcionamento)
<p>POST http://localhost:8080/categoria <br />
    {
        "nome": "Praça",
        "descricao":"ambiente publico aberto 24 horas",
        "possui_horario":"0"
    }
</p>
<p>GET http://localhost:8080/categoria</p>
<p>GET http://localhost:8080/categoria/{id}</p>
<p>PUT http://localhost:8080/categoria/{id} <br />
    {
        "nome": "icaro",
        "descricao":"descricao nova",
        "possui_horario":1
    }</p>
    
<p>DELETE http://localhost:8080/categoria/{id}</p>

<p>Ponto de interesse</p>
<p>POST http://localhost:8080/ponto_interesse <br />
    {
        "id_categoria":1,
        "nome": "shopping center",
        "descricao":"Shopping center, ambiente publico voltado a compra, venda e entretenimento",
        "coordenada_x":5,
        "coordenada_y":2,
        "abertura":"10:00",
        "fechamento": "18:00"
    }</p>
<p>GET http://localhost:8080/ponto_interesse</p>
<p>GET http://localhost:8080/ponto_interesse/{id}</p>
<p>GET http://localhost:8080/ponto_interesse/listar_proximos <br />
    {
        "coordenada_x":1,
        "coordenada_y":2,
        "metros":5,
        "hora":"17  :00"
    }</p>
<p>PUT http://localhost:8080/ponto_interesse/{id} <br />
    {
        "id_categoria":3,
        "nome": "Praça teste",
        "descricao":"ambiente publico aberto 24 horas",
        "coordenada_x":1,
        "coordenada_y":2,
        "abertura":"10:00",
        "fechamento": "18:00"
    }</p>
<p>DELETE http://localhost:8080/ponto_interesse/{id}</p>

### Testes
<p>Para rodar os testes sera necessario acessar acessar o conteiner e executalos<p>

<p>
Para acesar o conteiner utilize os comandos a baixo <br />
 > docker ps (pegue o id do conteiner)<br />
 > docker exec -it {id do conteiner} bash<br />
<br />
O bash do conteiner vai abrir, acesse o diretório do projeto e execute o comando 
> composer run test



</p>