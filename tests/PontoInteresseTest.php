<?php 
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Services\PontoInteresseService;
use App\Action\Request;

final class PontoInteresseTest extends TestCase
{
    /** @test */
    public function test_SeNaoForPassadoParametros_GeraException(){
        $pontoInteresse = new PontoInteresseService();
        $this->expectException(\ArgumentCountError::class);
        $pontoInteresse->create();
    }

        /** @test */
        public function test_enviajsonincorreto_GeraException(){
        
            $body["nome"] = "Praça Tiradentes";
            $body["descricao"] = "Praça publica, homenagem tiradentes";
            $body["coordenada_x"] = 3;
            $body["coordenada_y"] = 1;
            $body["abertura"] = "";
            $body["fechamento"] = "";
    
            $request = new Request();
            $request->setBody($body);
    
    
            $categoriaService = new PontoInteresseService();
            $this->expectException(\Exception::class);
            $categoriaService->create($request,"response","args");
    
        }

        /** @test */
        public function test_enviajsonincorreto2_GeraException(){
        
            $body["nome"] = "Praça Tiradentes";
            $body["descricao"] = "Praça publica, homenagem tiradentes";
            $body["coordenada_x"] = 3;
            $body["coordenada_y"] = 1;
            $body["abertura"] = "a";
            $body["fechamento"] = "3";
    
            $request = new Request();
            $request->setBody($body);
    
    
            $categoriaService = new PontoInteresseService();
            $this->expectException(\Exception::class);
            $categoriaService->create($request,"response","args");
    
        }

         /** @test */
         public function test_pontoInteressePonssuiHorario_GerarErroCasoNaoSejaInformado(){
        
            $body["id_categoria"] = 7;
            $body["nome"] = "Shopping Center";
            $body["descricao"] = "Shopping center";
            $body["coordenada_x"] = 3;
            $body["coordenada_y"] = 1;
            $body["abertura"] = "";
            $body["fechamento"] = "";
    
            $request = new Request();
            $request->setBody($body);
    
            $pontoInteresse = new PontoInteresseService();
            $this->expectException(\Exception::class);
            $pontoInteresse->create($request,"response","args");

        }


        /*
         para realizar esse teste, o body precisa de um id categoria valido. ou seja uma categoria existente. (por esse motivo ele está comentado, apos inserir os
         valores corretos pode descomentar e executar)
        */
        
        /** @test */
    //     public function test_enviajsoncomdadoscorretos(){

    //     $body["id_categoria"] = 88;
    //     $body["nome"] = "Shopping Center";
    //     $body["descricao"] = "Shopping center";
    //     $body["coordenada_x"] = 3;
    //     $body["coordenada_y"] = 1;
    //     $body["abertura"] = "08:00";
    //     $body["fechamento"] = "17:00";

    //     $request = new Request();
    //     $request->setBody($body);

    //     $pontoInteresse = new PontoInteresseService();
    //     $retorno = $pontoInteresse->create($request,"response","args");

    //     $this->assertStringContainsString("id", $retorno);

    // }

     /** @test */
     public function test_listaGerada(){
        $pontoInteresse = new PontoInteresseService();
        
        $retorno = json_decode($pontoInteresse->get('','',''));

        $this->assertGreaterThan(0,count($retorno));

    }

    /** @test */
    public function test_atualizar_verifica_parametros(){
        

        $body["id_categoria"] = 7;
        $body["nome"] = "Shopping Center";
        $body["descricao"] = "Shopping center";
        $body["coordenada_x"] = 3;
        $body["coordenada_y"] = 1;
        $body["abertura"] = "08:00";
        $body["fechamento"] = "17:00";

 
        $request = new Request();
        $request->setBody($body);
        $pontoInteresse = new PontoInteresseService();
        $this->expectException(\ArgumentCountError::class);
        $pontoInteresse->update($request,'');

    }

    /** @test */
    public function test_atualizar_enviajsoncomdadoserrados_GeraException(){
        

        $body["id_categoria"] = "ola";
        $body["nome"] = "Shopping Center";
        $body["descricao"] = "Shopping center";
        $body["coordenada_x"] = "asdf";
        $body["coordenada_y"] = 2;
        $body["abertura"] = "asdf";
        $body["fechamento"] = "17:00";

 
        $request = new Request();
        $request->setBody($body);
        $pontoInteresse = new PontoInteresseService();

        $this->expectException(\Exception::class);

        $pontoInteresse->update($request,'',"args");

    }

    public function test_deletarsemid_GerarException(){
        
        $pontoInteresse = new PontoInteresseService();

        $this->expectException(\Exception::class);

        $pontoInteresse->delete('','',"args");

    }

    public function test_calcularDistancia_retornaTrue(){

        $ObjpontoInteresse = new stdClass();
        $ObjpontoInteresse->coordenada_x = 10;
        $ObjpontoInteresse->coordenada_y = 25;

        $body['coordenada_x'] = 8;
        $body['coordenada_y'] = 20;
        $body['metros'] = 10;

        $pontoInteresse = new PontoInteresseService();
        $this->assertTrue($pontoInteresse->calcularDistancia($ObjpontoInteresse,$body));
    }

    public function test_calcularDistancia_retornaFalse(){

        $ObjpontoInteresse = new stdClass();
        $ObjpontoInteresse->coordenada_x = 10;
        $ObjpontoInteresse->coordenada_y = 25;

        $body['coordenada_x'] = 8;
        $body['coordenada_y'] = 20;
        $body['metros'] = 5;

        $pontoInteresse = new PontoInteresseService();
        $this->assertFalse($pontoInteresse->calcularDistancia($ObjpontoInteresse,$body));
    }

    public function test_buscarproximos(){

        $ObjpontoInteresse = new stdClass();
        $ObjpontoInteresse->coordenada_x = 10;
        $ObjpontoInteresse->coordenada_y = 25;

        $body['coordenada_x'] = 8;
        $body['coordenada_y'] = 20;
        $body['metros'] = 5;

        $pontoInteresse = new PontoInteresseService();
        $retorno = $pontoInteresse->buscarProximos($ObjpontoInteresse,$body);
        $this->assertIsArray($retorno);

    }
  
   
}