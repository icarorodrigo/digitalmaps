<?php 
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Services\CategoriaService;
use App\Action\Request;

final class CategoriaTest extends TestCase
{
    /** @test */
    public function test_SeNaoForPassadoParametros_GeraException(){
        $categoriaService = new CategoriaService();
        $this->expectException(\ArgumentCountError::class);
        $categoriaService->create();
    }
    /** @test */
    public function test_enviajsoncomdadoserrados_GeraException(){
        
        $body["nome"] = "Parques";
        $body["descricao"] = "Parques publicos, com area de lazer e comercio";
        $body["possui_horario"] = "asdf";

        $request = new Request();
        $request->setBody($body);


        $categoriaService = new CategoriaService();
        $this->expectException(\Exception::class);
        $categoriaService->create($request,"response","args");

    }
    /*
        Esse teste realiza a inserção de uma nova categoria, para utilizado basta descomentar o código.
    */
    /** @test */
    // public function test_enviajsoncomdadoscorretos(){

    //     $body["nome"] = "Parques";
    //     $body["descricao"] = "Parques publicos, com area d";
    //     $body["possui_horario"] = 0;

 
    //     $request = new Request();
    //     $request->setBody($body);
        
    //     $categoriaService = new CategoriaService();

    //     $retorno = $categoriaService->create($request,"response","args");

    //     $this->assertStringContainsString("id", $retorno);

    // }

    /** @test */
    public function test_listaGerada(){
        $categoriaService = new CategoriaService();
        
        $retorno = json_decode($categoriaService->get('','',''));

        $this->assertGreaterThan(0,count($retorno));

    }

    /** @test */
    public function test_atualizar_verifica_parametros(){
        
        $body["nome"] = "Parques";
        $body["descricao"] = "Parques publicos, com area d";
        $body["possui_horario"] = 0;

 
        $request = new Request();
        $request->setBody($body);
        $categoriaService = new CategoriaService();
        $this->expectException(\ArgumentCountError::class);
        $categoriaService->update($request,'');

    }

    /** @test */
    public function test_atualizar_enviajsoncomdadoserrados_GeraException(){
        
        $body["nome"] = "Parques";
        $body["descricao"] = "Parques publicos, com area d";
        $body["possui_horario"] = 'asdf';

 
        $request = new Request();
        $request->setBody($body);
        $categoriaService = new CategoriaService();

        $this->expectException(\Exception::class);

        $categoriaService->update($request,'',"args");

    }

    public function test_deletarsemid_GerarException(){
        
        $categoriaService = new CategoriaService();

        $this->expectException(\Exception::class);

        $categoriaService->delete('','',"args");

    }
}